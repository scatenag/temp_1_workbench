# Softi Workbench

### This is the workbench for the acquisition of the code developed for the CEP by Tonina Starita in 1968, to smoothen a curve. 

Any [SWHAPPE](https://github.com/Unipisa/SWHAPPE) workbench comes with a few predefined folders, to support the users in the process. 

Folder [raw materials](./raw_materials) is for the original materials, as they have been found or submitted.

Folder [browsable source](./browsable_source) is for a browsable version of the source code. Source files, with right extention, have to be accessible through the GitHub web interface, e.g., archives should be decompressed, immages should be trascibed, etc.

Folder [source](./source) is for the curated revision of the source code, as a base for the recontruction of the development history as a git repository, i.e., a folder for each major version of the code.

Folder [metadata](/.metadata) holds various files with meta information used throughout the process. 

Please refear to the [SWHAPPE](https://github.com/Unipisa/SWHAPPE) guidelines for greater details. 
